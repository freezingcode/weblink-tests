package qa.test;

import java.util.HashMap;
import java.util.Map;

import org.testng.ITest;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import qa.test.Browser;
import qa.test.Element;
import qa.test.Log;
import qa.test.SauceREST;
import qa.test.Util;
import qa.test.Verification;
import qa.test.util.SeHelper;

/**
 * This file is a template.  You will create a similar class in your test project.
 */
public class WLITestCase implements ITest 
{	
	private String testName; 
	public SeHelper testHelper = new SeHelper(); 
	public Verification verification = testHelper.seVerification; 
	public Log log = testHelper.seLog; 
	public Browser browser = testHelper.seBrowser; 
	public Element element = testHelper.seElement; 
	protected String dateStamp = Util.getDateStamp( "GMT-7" );
	public boolean testStatus;
	protected String b = "SauceFirefox";
	protected Browser.browser bro = Browser.browser.SauceFirefox;
	
	public void pushThisTestStatus() 
	{		
		String jobID = testHelper.getRemoteWebDriverSessionId();
		log.logInfo( "Pushing result for SauceLabs job '" + jobID + " : " + testStatus );
		Map<String, Object> updates = new HashMap<String, Object>();
		updates.put( "name", testHelper.getTestName() );
		updates.put( "passed", testStatus );
		updates.put( "build", testHelper.getBuild() );
		SauceREST client = new SauceREST( testHelper.getSauceUser(), testHelper.getSauceKey() );
		client.updateJobInfo( jobID, updates );
	}
	
	@BeforeMethod
	public void setUp() {
		log.logTcStep("...................................................");
		log.logTcStep("Test started");
		log.logTcStep("...................................................");
		
		testHelper.setTestName( this.getClass().getSimpleName() + "_" + b );
		log.logTcStep( "Test case name: " + testName );
		log.setLogTcName( testName );
		testHelper.setTestName( this.getClass().getSimpleName() + "_" + b );
		testHelper.setTagsByString(b);
		testHelper.setBuildNumber("Unknown");
		testHelper.newDriver( bro );
	}
	
	@AfterMethod (alwaysRun=true)
	public void tearDown() {
		Util.sleep(2000);		
		verification.checkForFail();
		setResultStatus( verification.saucePassed );
		pushThisTestStatus();
		browser.quit();	
		
		log.logTcStep("...................................................");
		log.logTcStep("... Test passed: " + this.testStatus );
		log.logTcStep("...................................................");
	}
	
	public void setResultStatus( boolean status ) 
	{
		this.testStatus = status;		
	}

	@Override
	public String getTestName() {
		if ( this.testName == null ) {
			return "N/A";
		}
		return this.testName;
	}
	
	/**
	 * Must set test name in a @Before configuration method, if possible.
	 * @param name
	 */
	public void setTestName( String name ) {
		this.testName = name;
	}
	
}
