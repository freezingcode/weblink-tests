package qa.test;

import java.util.ArrayList;

import qa.test.util.SeHelper;

public class WLIWebDriverUtil extends SeHelper {

	public String getURL() 
	{		
		return seBrowser.getCurrentUrl();		
	}
	
	public void sleep( double seconds ) {
		try {
			long mill = (long) (seconds * 1000);
			seLog.logInfo( "Sleeping for '" + seconds + "' seconds." );
		    Thread.sleep( mill );
		} catch ( InterruptedException ex ) {
		    Thread.currentThread().interrupt();
		}
	}
	
	public static ArrayList<String> WLIDomainList() {
		
		@SuppressWarnings("serial")
		ArrayList<String> objListDomains = new ArrayList<String>(){{
			//add("http://www.vtchamber.com");
			//add("http://www.visitvt.com");
			add("http://www.ibrooklyn.com");
			add("http://web.glenwoodchamber.com");
			add("http://members.indychamber.com");
		}};
		
		return objListDomains;
	}
	
}
