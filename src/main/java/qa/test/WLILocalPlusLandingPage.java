package qa.test;

import org.openqa.selenium.By;

import qa.test.templates.ProjectSuperHelper;
import qa.test.util.SeHelper;

public class WLILocalPlusLandingPage extends ProjectSuperHelper {
	
	private final By landingSearchField = By.xpath( ".//*[@id='searchterm']" );
	//TODO Some sites use a different id:       content_content1_Directory1_DirectorySearch1_SearchKeyword_txtKeyword
	private final By searchField = By.xpath( ".//*[@id='content1_Directory1_DirectorySearch1_SearchKeyword_txtKeyword']" );
	private final By landingSiteSearchButton = By.xpath( ".//*[@id='searchsubmit']" );
	//TODO Some sites use a different id:        content_content1_Directory1_DirectorySearch1_btnSimpleSearch
	private final By searchButton = By.xpath( ".//*[@id='content1_Directory1_DirectorySearch1_btnSimpleSearch']" );

	public WLILocalPlusLandingPage( SeHelper helper ) {
		super( helper );
	}
	
	public void clickLandingSearchField() {
		hElement.clickElement( searchField );
	}
	
	public void enterLandingSearchString( String textToType ) {
		hElement.enterText( landingSearchField, textToType);
	}
	
	public void enterSearchString( String textToType ) {
		hElement.enterText( searchField, textToType);
	}
	
	public void clickLandingSiteSearchButton() {
		hElement.clickElement( landingSiteSearchButton );
	}
	
	public void clickSearchButton() {
		hElement.clickElement( searchButton );
	}

}
