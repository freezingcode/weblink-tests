package qa.tests;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import qa.test.WLILocalPlusLandingPage;
import qa.test.WLITestCase;
import qa.test.WLIWebDriverUtil;

public class TestWebLinkLocalPlusLandingPageTitle extends WLITestCase 
{	
	
	@Test( enabled = true, description = "Navigate to '/search' uri and verify page title.", timeOut = 300000 )
	public void testWebLinkLocalPlusLandingPageTitle( Method method ) 
	{
		log.logTcStep("Test started: " + method.getName());
		
		for (int i = 0; i < WLIWebDriverUtil.WLIDomainList().size(); i++) {
			browser.navigateTo(WLIWebDriverUtil.WLIDomainList().get(i) + "/search");
			
			//Verify the page navigates and the new page title contains "Directory"
			verification.verifyContains(testHelper.getTestName(), browser.getWindowTitle(), "Directory");
			
			element.setSeTimeOut(2000);
		}
		
		verification.checkForFail(testHelper.getTestName());
	}
	
}
