package qa.tests;

import java.lang.reflect.Method;

import org.testng.annotations.Test;

import qa.test.WLILocalPlusLandingPage;
import qa.test.WLITestCase;
import qa.test.WLIWebDriverUtil;

public class TestWebLinkLocalPlusLandingPageSearchByKeyword extends WLITestCase 
{	

	@Test( enabled = true, description = "Navigate to '/search' uri, Search by Keyword, and verify page title.", timeOut = 300000 )
	public void testWebLinkLocalPlusLandingPageSearchByKeyword( Method method ) 
	{
		log.logTcStep("Test started: " + method.getName());
		
		for (int i = 0; i < WLIWebDriverUtil.WLIDomainList().size(); i++) {
			
			WLILocalPlusLandingPage objPage = new WLILocalPlusLandingPage(testHelper);
			
			String strURL = WLIWebDriverUtil.WLIDomainList().get(i) + "/search";
			browser.navigateTo(strURL);
			
			//Enter text in search field 
			objPage.enterSearchString("Lodging");
			//Click Search button
			objPage.clickSearchButton();
			//Verify the directory results page title contains "Lodging"
			verification.verifyContains(testHelper.getTestName(), browser.getWindowTitle(), "Lodging");
			
			element.setSeTimeOut(2000);
		}
		
		verification.checkForFail(testHelper.getTestName());
	}
	
}
